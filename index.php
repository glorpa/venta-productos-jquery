<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Untitled</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="container-fluid">
		<div class="row mt-4">
			<div class="col-12 text-center" >
				<h1>venta de articulos</h1>
			</div>
		</div>
		<form action="index.php" class="formularioventas" method="POST">
			<div class="row">
				<div class="col-6 text-right">
					elija reproductor
				</div>
				<div class="col-6">
					<select name="" id="ddlmenu">
						<option class="opcionmenu" value="nulo">Seleccione...</option>
						<option class="opcionmenu" value="mp3">mp3</option>
						<option
						class="opcionmenu"
						 value="mp4">mp4</option>
						<option
						class="opcionmenu"
						 value="ipod">ipod</option> 
					</select>
				</div>
             </div>
			<div class="row mt-4">
				<div class="col-md-6
				text-right">
					valor		
			</div>
			<div class="col-md-6">
				<input type="text" readonly=""id="txtvalor">
			</div>
		</div>
		<div class="row mt-4">
		<div class="col-md-6
				text-right">
					Cantidad	
			</div>

			<div class="col-md-6">
				<input type="number"
				min="1"
				id="txtcantidad">
				
		    </div>			
			
		</div>
		<div class="row mt-4">
			<div class="col-md-6 text-right">
					Recargo		
            </div>
            <div class="col-md-6">
            	<input type="radio" name="porcentaje" value="5"
            	checked="">5%
            	<input type="radio" name="porcentaje" value="10"class="ml-2">10%
            </div>	
        </div>
        <div class="row mt-4">
        	<div class="col-md-6 text-right">
        		Subtotal
        	</div>
        	<div class="col-md-6">
        		<input type="text" readonly="" id="txtSubtotal">
            </div>		
       	</div>
        		
        	<div class="row mt-4">
        		<div class="col-6 text-right">
        	
        		Total Recargo
        	</div>
        	<div class="col-md-6">
        		<input type="text" readonly=""id="txtTotalRecargo">
           	</div>
       	</div>
        <div class="row mt-4">
        	<div class="col-md-6 text-right">
        		Total        
        	</div>
            <div class="col-md-6">
            <input type="text" readonly="" id="txtTotal">	
        </div>
     </div>		
		<div class="row mt-4">
			<div class="col-12
			text-center">
			<input type="button" id="btnMostrar" class="btn btn-dark
			directBtn"
			value="Mostrar
			Totales">
        </div>
     </div>
</form>		  
</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="js/codigo.js"></script>
</body>
</html>